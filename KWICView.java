import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle;


public class KWICView extends JFrame implements ActionListener{

	public static final String Ignore_Word_Desc1 = "Please input words to ignore:";
	public static final String Ignore_Word_Desc2 = "Format: \"word1\",\"word2\",\"...\"";
	public static final String Input_Word_Desc1 = "Please input words to process:";
	public static final String Input_Word_Desc2 = "Format: \"word1\",\"word2\",\"...\"";

	public static final int Gridlayout_Rows = 0;
	public static final int Gridlayout_Cols = 2;
	public static final int Gridlayout_Height = 20;
	public static final int Gridlayout_Vertical = 10;
	public static final int TextField_Length = 30;
	public static final int Window_Height = 800;
	public static final int Window_Width = 550;
	public static final int GAP_Distance = 100;
	public static final int Left_Margin = 25;
	public static final int Right_Margin = 25;
	public static final int Top_Margin = 25;
	public static final int Text_Margin = 20;

	KWICController controller;

	//GUI defind
	JPanel KWIC_Panel;
	JButton process;
	JButton reset;
	JButton exit;
	JLabel ignore_des1;
	JLabel input_des1;
	JLabel ignore_des2;
	JLabel input_des2;
	JTextArea ignore_text;
	JTextArea input_text;
	JTextArea result;
	JScrollPane ignore_text_scroll;
	JScrollPane input_text_scroll;
	JScrollPane result_text_scroll;


	public KWICView(){

		KWIC_Panel = new JPanel();

		process = new JButton("Process");
		process.addActionListener(this);

		reset = new JButton("Reset");
		reset.addActionListener(this);

		exit = new JButton("Exit");
		exit.addActionListener(this);

		ignore_des1 = new JLabel(Ignore_Word_Desc1);
		input_des1 = new JLabel(Input_Word_Desc1);
		ignore_des2 = new JLabel(Ignore_Word_Desc2);
		input_des2 = new JLabel(Input_Word_Desc2);
		

         
		ignore_text = new JTextArea("");
		ignore_text.setBorder(BorderFactory.createLineBorder(Color.black));
		ignore_text.setLineWrap(true);
		ignore_text_scroll = new JScrollPane(ignore_text);
		
		

		input_text = new JTextArea("");
		input_text.setBorder(BorderFactory.createLineBorder(Color.black));
		input_text.setLineWrap(true);
		input_text_scroll = new JScrollPane(input_text);

		result = new JTextArea();
		result.setBorder(BorderFactory.createLineBorder(Color.black));
		result.setLineWrap(false);
		result_text_scroll = new JScrollPane(result);
		

		GroupLayout layout = new GroupLayout(KWIC_Panel);
		KWIC_Panel.setLayout(layout);
		layoutDesign(layout);
		this.add(KWIC_Panel);



	}
	@Override
	public void actionPerformed(ActionEvent event){
		if(event.getSource() == process){
			if (controller.processIgnoreWords()) {
				if (controller.processInputWords()) {
					controller.updateProcess();
				}
			}
		}
		else if(event.getSource() == reset){
			controller.resetSystem();
		}
		else if(event.getSource() == exit){
			System.exit(0);
		}
	}
	
	public void layoutDesign(GroupLayout layout){
		layout.setHorizontalGroup(
				layout.createSequentialGroup()
				.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED,
						GroupLayout.DEFAULT_SIZE, Left_Margin)


						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)

								.addGroup(
										layout.createSequentialGroup()

										.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
												.addComponent(ignore_des1)
												.addComponent(ignore_des2)
												.addComponent(ignore_text_scroll)
												)
												.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED,
														GroupLayout.DEFAULT_SIZE, GAP_Distance)
														.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
																.addComponent(input_des1)
																.addComponent(input_des2)
																.addComponent(input_text_scroll)
																))
																.addComponent(result_text_scroll)
																.addGroup(layout.createSequentialGroup()
																		.addComponent(process)
																		.addComponent(reset)
																		.addComponent(exit)

																		)

								)
								.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED,
										GroupLayout.DEFAULT_SIZE, Right_Margin)
				);

		layout.setVerticalGroup(
				layout.createSequentialGroup()
				.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,
						GroupLayout.DEFAULT_SIZE, Top_Margin)
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(ignore_des1)
								.addComponent(input_des1))

								.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
										.addComponent(ignore_des2)
										.addComponent(input_des2))

										.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED,GroupLayout.DEFAULT_SIZE, Text_Margin)
										.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
												.addComponent(ignore_text_scroll)
												.addComponent(input_text_scroll))
												.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,GroupLayout.DEFAULT_SIZE, Text_Margin)

												.addComponent(result_text_scroll)
												.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,GroupLayout.DEFAULT_SIZE, Text_Margin)
												.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
														.addComponent(process)
														.addComponent(reset)
														.addComponent(exit))

				);
	}

}
