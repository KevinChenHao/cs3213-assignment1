import java.awt.Frame;
import java.io.IOException;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;
import javax.swing.JOptionPane;



public class KWICModel {
	private Stack<String> title;
	private Set<String> words_ignore;
	private boolean valid_input;
	KWICView view;
	KWICController controller;

	public KWICModel(){
		//initialize data
		words_ignore = new TreeSet<String>();
		title = new Stack<String>();
	}


	//Manipulation from controller
	boolean processIgnoreWords(String [] words){
		int st,en;
		valid_input = true;
		for (int i = 0; i<words.length;i++) {
			try{
				validateInput(words[i]);
				st = words[i].indexOf('"');
				words[i] = words[i].substring(st + 1);
				en = words[i].indexOf('"');
				words[i] = words[i].substring(0,en);
				words_ignore.add(words[i].toLowerCase());
			}catch (Exception e) {
				words_ignore.clear();
				break;
			}
		}
		return valid_input;
	}


	boolean processInputWords(String[] words){
		int st,en;
		for (int i = 0; i<words.length;i++) {
			try{
				validateInput(words[i]);
				st = words[i].indexOf('"');
				words[i] = words[i].substring(st + 1);
				en = words[i].indexOf('"');
				words[i] = words[i].substring(0,en);
				title.push(words[i]);
			}catch (Exception e){
				title.clear();
				break;
			}
		}
		return valid_input;
	}


	public Queue<String> process(){
		Queue<String> resultWords = new PriorityQueue<String>(20,new stringcompare());
		String currTitle;
		int i;
		String[] parts = null;
		while (!title.empty()) {
			parts = null;
			currTitle = (String) title.pop();
			currTitle = currTitle.trim();
			parts = currTitle.split(" ");
			currTitle = "";
			for (i = 0; i< parts.length; i ++) {
				if (words_ignore.contains(parts[i].toLowerCase())) {
					parts[i] = parts[i].toLowerCase();
				}
				currTitle = currTitle + " " + parts[i];
			}
			currTitle = currTitle.trim();
			for (i = 0; i< parts.length; i ++) {
				if (words_ignore.isEmpty() || !words_ignore.contains(parts[i].toLowerCase())) {
					resultWords.add(currTitle);
				}
				if (parts.length >1) {
					currTitle = currTitle.substring(parts[i].length() + 1);
					currTitle = currTitle + " " + parts[i];
				}
			}
		}

		return resultWords;
	}


	class stringcompare implements Comparator<String> {
		public int compare(String s1, String s2){
			return s1.compareTo(s2);
		}
	}

	public void validateInput(String input) throws Exception {
		input=input.trim();
		//case 1 empty
		if(!input.isEmpty()){
			if(!(input.charAt(0) == '"' && input.charAt(input.length()-1) == '"')){
				JOptionPane.showMessageDialog(new Frame(),
						"Invalid Format",
						"Inane error",
						JOptionPane.ERROR_MESSAGE);
				valid_input = false;
				throw new IOException();
			}
		}
	}

	//update view
	void updateProcess(){
		StringBuilder resultText = new StringBuilder();
		Queue<String> resultWords = controller.process();
		
		
		String tmp = resultWords.poll();
		String first = tmp.substring(0,1).toUpperCase();
		
		resultText = resultText.append(first);
		resultText = resultText.append(tmp.substring(1));
		
		while(resultWords.peek() != null){
			System.out.print(resultWords.peek());
			
			resultText = resultText.append("\n");
			tmp = resultWords.poll();
			first = tmp.substring(0,1).toUpperCase();
			
			resultText = resultText.append(first);
			resultText = resultText.append(tmp.substring(1));
		}

		String finalString = new String(resultText);
		if(finalString.compareTo("null") == 0)
			finalString = "";
		view.result.setText(finalString);
	}

	void resetSystem(){
		view.ignore_text.setText("");
		view.input_text.setText("");
		view.result.setText("");
	}
}
