import java.util.Queue;
import javax.swing.JFrame;



public class KWICController {

	public static final int Window_Height = 800;
	public static final int Window_Width = 550;

	KWICView view;
	KWICModel model;

	public KWICController(){
		view = new KWICView();
		view.setTitle("KWIC System");
		view.setSize(Window_Width,Window_Height);
		view.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		view.setVisible(true);
		view.controller = this;

		model = new KWICModel();
		model.controller = this;
		model.view = this.view;
	}



	boolean processIgnoreWords(){
		//ask model to update words
		String [] words = view.ignore_text.getText().split(",");
		return model.processIgnoreWords(words);
	}

	boolean processInputWords(){
		//ask model to update input words
		String [] words = view.input_text.getText().split(",");
		
		return model.processInputWords(words);
	}
	Queue<String> process(){
		return model.process();
	}

	void updateProcess(){
		model.updateProcess();
	}
	void resetSystem(){
		model.resetSystem();
	}

	public static void main (String[] args){
		KWICController controller = new KWICController();

	}
}
